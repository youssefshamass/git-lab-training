﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer
{
    class Customer
    {
        private String customerName;

        public Customer(String name)
        {
            customerName = name;
        }

        public String getName()
        {
            return customerName;
        }
    }
}
